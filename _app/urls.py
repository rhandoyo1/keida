from _app.views import home, about, contact, faq
from django.urls import path

urlpatterns = [
    path('', home), 
    path('about', about), 
    path('contact', contact), 
    path('faq', faq), 
]
