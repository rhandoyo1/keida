from django.db import models
from django.utils.text import slugify

# Create your models here.

class Products(models.Model):
    name = models.CharField(max_length=100)
    desc = models.TextField()
    price = models.CharField(max_length=50)
    image = models.ImageField(upload_to='ImgProduct', default='')
    slug = models.SlugField(blank=True, null=True, editable=False)

    def __str__(self):
        return self.name
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)

