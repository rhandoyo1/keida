from django.shortcuts import render
from .models import Products

# Create your views here.

def home(request):
    products = Products.objects.all()

    context = {
        'title':home,
        'products':products,

    }
    return render(request,'home.html',context )


def about(request):
    context = {
        'title':about,
    }

    return render(request,'about.html',context )

def contact(request):
    context = {
        'title':contact,
    }

    return render(request,'contact.html',context )

def faq(request):
    context = {
        'title':faq,
    }

    return render(request,'faq.html',context )